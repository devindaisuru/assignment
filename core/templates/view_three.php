<div class="row justify-content-md-center wizard-container">
  <div class="col col-md-4 align-self-center">
    <form action="/third" method="post">
      <h2>Payment Info:</h2>
      <div class="form-group">
        <label>Account Owner</label>
        <input name="acc_owner" type="text" class="form-control" value="<?php echo (isset($params['acc_owner'])?$params['acc_owner']:''); ?>" placeholder="Account Owner"/>
      </div>
      <div class="form-group">
        <label>IBAN</label>
        <input name="iban" type="text" class="form-control" value="<?php echo (isset($params['iban'])?$params['iban']:''); ?>" placeholder="IBAN"/>
      </div>
      <input name="step" type="hidden" value="3"/>
      <button type="submit" name="previous" value="2" class="btn btn-primary">Prev</button>
      <button type="submit" class="btn btn-primary">Next</button>
    </form>
  </div>
</div>