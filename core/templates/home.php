<div class="row justify-content-md-center wizard-container">
  <div class="col col-md-4 align-self-center">
    <form action="/first" method="post">
      <div class="form-group">
        <label>Firstname</label>
        <input required name="firstname" type="text" class="form-control" value="<?php echo (isset($params['firstname'])?$params['firstname']:''); ?>" placeholder="Firstname"/>
      </div>
      <div class="form-group">
        <label>Lastname</label>
        <input required name="lastname" type="text" class="form-control" value="<?php echo (isset($params['lastname'])?$params['lastname']:''); ?>" placeholder="Lastname"/>
      </div>
      <div class="form-group">
        <label>Email</label>
        <input required name="email" type="email" class="form-control" value="<?php echo (isset($params['email'])?$params['email']:''); ?>" placeholder="Email"/>
      </div>
      <input name="step" type="hidden" value="1"/>
      <button type="submit" class="btn btn-primary">Next</button>
    </form>
  </div>
</div>