<div class="row justify-content-md-center wizard-container">
  <div class="col col-md-4 align-self-center">
    <form action="/second" method="post">
      <h2>Address</h2>
      <div class="form-group">
        <label>Street</label>
        <input name="street" type="text" class="form-control" value="<?php echo (isset($params['street'])?$params['street']:''); ?>" placeholder="Street"/>
      </div>
      <div class="form-group">
        <label>House Number</label>
        <input name="house" type="text" class="form-control" value="<?php echo (isset($params['house_number'])?$params['house_number']:''); ?>" placeholder="House Number"/>
      </div>
      <div class="form-group">
        <label>ZIP Code</label>
        <input name="zip" type="text" class="form-control" value="<?php echo (isset($params['zip'])?$params['zip']:''); ?>" placeholder="ZIP Code"/>
      </div>
      <div class="form-group">
        <label>City</label>
        <input name="city" type="text" class="form-control" value="<?php echo (isset($params['city'])?$params['city']:''); ?>" placeholder="City"/>
      </div>
      <input name="step" type="hidden" value="2"/>
      <button type="submit" name="previous" value="1" class="btn btn-primary">Prev</button>
      <button type="submit" class="btn btn-primary">Next</button>
    </form>
  </div>
</div>