<?php

/* added an autoload rather than including every thing */

function autoload($name) {
    if(file_exists(__DIR__ . DIRECTORY_SEPARATOR . $name . '.php')){
        include(__DIR__ . DIRECTORY_SEPARATOR . $name . '.php');
    }elseif(file_exists(CONTROLLERS_PATH . $name . '.php')){
        include(CONTROLLERS_PATH . $name . '.php');
    }elseif(file_exists(REQUEST_PATH . $name . '.php')){
        include(REQUEST_PATH . $name . '.php');
    }elseif(file_exists(MODELS_PATH . $name . '.php')){
        include(MODELS_PATH . $name . '.php');
    }
}

spl_autoload_register("autoload");
?>