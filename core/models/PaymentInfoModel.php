<?php
/*
* Payment info model
*/
class PaymentInfoModel extends DB{
    private $table = 'payment_info';

    public function save($info){
        $this->insert($this->table, $info);
        return $this->id();
    }

    public function getInfoByUserID($id){
        $result = $this->select($this->table, ['user_id' => $id], false, false, ['payment_id']);
        return $this->row_array();
    }
}