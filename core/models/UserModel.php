<?php

/*
* User Model
*/
class UserModel extends DB{
    private $table = 'users';

    public function getUserByID($id){
        $result = $this->select($this->table, ['id' => $id], false, false, ['user_id', 'page_index']);
        return $this->row_array();
    }
        
    public function save($user){
        $this->insert($this->table, $user);
        return $this->id();
    }

    public function update_user($id, $user){
        $this->update($this->table, $user, [
            'id' => $id
        ]);
    }
}