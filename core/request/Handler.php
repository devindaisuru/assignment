<?php

/*
 * This is directly called by the index to handle the requests
 */

/**
 * Description of handler
 *
 * @author devinda
 */
class Handler {
    private $server = null;
    public $request = [];
    public function __construct() {
        $this->request['get'] = $_GET;
        $this->request['post'] = $_POST;
        $this->request['cookie'] = $_COOKIE;
        $this->server = $_SERVER;
    }
    
    public function handle(){
        try{
            $call_for = $this->server['REQUEST_URI'];
    
            // handles discrete requests
            switch ($call_for){
                case '/':
                case '/home':
    
                    // validated the request method so that we can confirm what we are serving
                    if($this->server['REQUEST_METHOD'] != "GET"){
                        http_response_code(404);
                        break;
                    }
                    $homeController = new HomeController($this->request);
                    $homeController->handle();
                    break;
                case '/first':
                    if($this->server['REQUEST_METHOD'] != "POST"){
                        http_response_code(404);
                        continue;
                    }
                    
                    $registerController = new RegisterOneController($this->request);
                    $registerController->handle();
                    break;
                case '/second':
                    if($this->server['REQUEST_METHOD'] != "POST"){
                        http_response_code(404);
                        continue;
                    }
                    
                    $registerController = new RegisterTwoController($this->request);
                    $registerController->handle();
                    break;
                case '/third':
                    if($this->server['REQUEST_METHOD'] != "POST"){
                        http_response_code(404);
                        continue;
                    }
                    
                    $registerController = new RegisterThreeController($this->request);
                    $registerController->handle();
                    break;
                default :
                    http_response_code(404);
                    break;
            }
            
        }catch(\Exception $e){
            error_log($e->getMessage());
            header('location: /');
        }
    }
}