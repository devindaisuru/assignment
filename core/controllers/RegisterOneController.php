<?php

/*
 * Handles the first form
 */

/**
 * Description of Register
 *
 * @author devinda
 */
class RegisterOneController extends BaseController{
    public function handle(){
        $userModel = new UserModel();

        // confirm that we are serving the right step
        if(isset($this->request['post']['step']) && $this->request['post']['step'] == "1"){
            if(isset($this->request['cookie']['user_id'])){
                $userID = $this->request['cookie']['user_id'];

                // update the user data when they are present
                $userModel->update_user($userID, [
                    'firstname' => isset($this->request['post']['firstname'])?$this->request['post']['firstname']:'',
                    'lastname' => isset($this->request['post']['lastname'])?$this->request['post']['lastname']:'',
                    'email' => isset($this->request['post']['email'])?$this->request['post']['email']:'',
                ]);
            }else{
                // update the save data when they are present
                $user_id = $userModel->save([
                    'firstname' => isset($this->request['post']['firstname'])?$this->request['post']['firstname']:'',
                    'lastname' => isset($this->request['post']['lastname'])?$this->request['post']['lastname']:'',
                    'email' => isset($this->request['post']['email'])?$this->request['post']['email']:'',
                    'status' => '0'
                ]);
                setcookie('user_id', $user_id);
            }
            
            // sets which page to load
            setcookie('page_index', 2);
        }

        header('location:/');
    }
}
