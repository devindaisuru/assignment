<?php

/*
 * Serves the third form submission
 */

/**
 * Description of Register
 *
 * @author devinda
 */
class RegisterThreeController extends BaseController{
    public function handle(){

        // handles the previous click
        if(isset($this->request['post']['previous'])){
            setcookie('page_index', $this->request['post']['previous']);

            header('location:/');
            return;
        }

        $userModel = new UserModel();
        if(isset($this->request['post']['step']) && $this->request['post']['step'] == "3"){
            $userID = $this->request['cookie']['user_id'];

            // save the payment info
            $user_id = $userModel->update_user($userID, [
                'acc_owner' => isset($this->request['post']['acc_owner'])?$this->request['post']['acc_owner']:'',
                'iban' => isset($this->request['post']['iban'])?$this->request['post']['iban']:''
            ]);

            // payment info is ready, process it
            $info = $this->process_payment([
                'customerId' => (int)$userID,
                'iban' => isset($this->request['post']['iban'])?$this->request['post']['iban']:'',
                'owner' => isset($this->request['post']['acc_owner'])?$this->request['post']['acc_owner']:''
            ]);
            
            $payment_info = new PaymentInfoModel();
            if(isset($info['paymentDataId'])){

                // save the payment result if it is completed
                $payment_info->save([
                    'payment_id' => $info['paymentDataId'],
                    'user_id' => $userID
                ]);

                // now the registration process is complete
                $user_id = $userModel->update_user($userID, [
                    'status' => '1'
                ]);
                setcookie('page_index', 4);
            }
        }

        header('location:/');
    }

    // call external API to get payment info
    private function process_payment($info){
        $ch = curl_init(PAYMENT_URL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($info));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

        $response = curl_exec($ch);
        
        curl_close($ch);
        
        return json_decode($response, true);
    }
}
