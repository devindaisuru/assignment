<?php

/*
 * This the base controller which handle the basic template structure of the system and the controllers
 */

/**
 * Description of BaseController
 *
 * @author devinda
 */
class BaseController {

    public $request = null;
    function __construct($params) {
        $this->request = $params;
    }
    
    protected function loadTemplate($name, $params = []){
        include TEMPLATES_PATH . 'header.php';
        include TEMPLATES_PATH . $name . '.php';
        include TEMPLATES_PATH . 'footer.php';
    }
}
