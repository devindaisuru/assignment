<?php

/*
 * serves second form submission
 */

/**
 * Description of Register
 *
 * @author devinda
 */
class RegisterTwoController extends BaseController{
    public function handle(){
        if(isset($this->request['post']['previous'])){
            setcookie('page_index', $this->request['post']['previous']);

            header('location:/');
            return;
        }
        
        $userModel = new UserModel();
        
        if(isset($this->request['post']['step']) && $this->request['post']['step'] == "2"){
            $userID = $this->request['cookie']['user_id'];
            $user_id = $userModel->update_user($userID, [
                'street' => isset($this->request['post']['street'])?$this->request['post']['street']:'',
                'house_number' => isset($this->request['post']['house'])?$this->request['post']['house']:'',
                'zip' => isset($this->request['post']['zip'])?$this->request['post']['zip']:'',
                'city' => isset($this->request['post']['city'])?$this->request['post']['city']:'',
            ]);
            setcookie('page_index', 3);
        }

        header('location:/');
    }
}
