<?php

/*
 * This class is to handle the loanding page and loading various pages according to the requirement
 */

/**
 * Description of Home
 *
 * @author devinda
 */
class HomeController extends BaseController{

    public function handle(){
        $reg_data = [];
        
        if(isset($this->request['cookie']['page_index'])){
            $page_index = $this->request['cookie']['page_index'];
            $user = [];
            $info = [];
            if(isset($this->request['cookie']['user_id'])){
                $userID = $this->request['cookie']['user_id'];
                $userModel = new UserModel();
                $user = $userModel->getUserByID($userID);
                $payment_info = new PaymentInfoModel();
                $info = $payment_info->getInfoByUserID($userID);
            }
            
            if($user['status'] == "1"){
                $this->loadTemplate('view_four', $info);
            }elseif($this->request['cookie']['page_index'] == "1"){
                $this->loadTemplate('home', $user);
            }elseif($this->request['cookie']['page_index'] == "2"){
                $this->loadTemplate('view_two', $user);
            }elseif($this->request['cookie']['page_index'] == "3"){
                $this->loadTemplate('view_three', $user);
            }elseif($this->request['cookie']['page_index'] == "4"){
                $this->loadTemplate('view_four');
            }
        }else{
            $this->loadTemplate('home');
        }
    }
}
