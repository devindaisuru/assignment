<?php

/* 
 * Defines high level of the constatnts that application is hoping to use
 */

define('CONTROLLERS_PATH', __DIR__ . DIRECTORY_SEPARATOR . 'controllers/');
define('MODELS_PATH', __DIR__ . DIRECTORY_SEPARATOR . 'models/');
define('TEMPLATES_PATH', __DIR__ . DIRECTORY_SEPARATOR . 'templates/');
define('REQUEST_PATH', __DIR__ . DIRECTORY_SEPARATOR . 'request/');
define('MYSQL_HOST', 'localhost');
define('MYSQL_DB', 'register_db');
define('MYSQL_USER', 'reguser');
define('MYSQL_PASS', '1qaz!QAZ');
define('PAYMENT_URL', 'https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data');